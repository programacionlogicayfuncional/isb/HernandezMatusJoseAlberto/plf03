(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [xs] (- 5 xs))
        h (comp f g)]
    (h 7)))

(función-comp-1)

(defn función-comp-2
  []
  (let [f (fn [x] (+ 8 x))
        g (fn [xs] (- 2 xs))
        h (comp f g)]
    (h 1)))

(función-comp-2)

(defn función-comp-3
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [xs] (filter even? xs))
        h (fn [xx] (map inc xx))
        z (comp f g h)]
    (z #{-20 10 -3 5 -4 8 4 7 -10 12})))

(función-comp-3)

(defn función-comp-4
  []
  (let [f (fn [x] (into [] (remove pos? x)))
        g (fn [xs] (reverse xs))
        h (fn [xx] (map inc xx))
        z (comp f g h)]
    (z [1 2 -5 4 -9 -6 -8])))

(función-comp-4)

(defn función-comp-5
  []
  (let [f (fn [x] (list? x))
        g (fn [xs] (map-entry? xs))
        h (comp f g)]
    (h '(2 3 4 5))))

(función-comp-5)

(defn función-comp-6
  []
  (let [f (fn [x] (keyword x))
        g (fn [xs] (str xs))
        h (comp f g)]
    (h [1 2])))

(función-comp-6)

(defn función-comp-7
  []
  (let [f (fn [x] (second x))
        g (fn [xs] (reverse xs))
        h (comp f g)]
    (h '("a" 2 7 "b"))))

(función-comp-7)

(defn función-comp-8
  []
  (let [f (fn [x] (zero? x))
        g (fn [xs] (not xs))
        h (comp f g)]
    (h 5)))

(función-comp-8)

(defn función-comp-9
  []
  (let [f (fn [x] (not x))
        g (fn [xs] (zero? xs))
        h (comp f g)]
    (h 5)))

(función-comp-9)

(defn función-comp-10
  []
  (let [f (fn [x] (str x))
        g (fn [xs] (double xs))
        h (comp f g)]
    (h (+ 3 3 3))))

(función-comp-10)

(defn función-comp-11
  []
  (let [f (fn [x] (char? x))
        g (fn [xs] (first xs))
        h (comp f g)]
    (h "hola")))

(función-comp-11)

(defn función-comp-12
  []
  (let [f (fn [x] (ident? x))
        g (fn [xs] (empty xs))
        h (comp f g)]
    (h [1 2 3])))

(función-comp-12)

(defn función-comp-13
  []
  (let [f (fn [x] (indexed? x))
        g (fn [xs] (vector xs))
        h (comp f g)]
    (h {:a 1 :b 2})))

(función-comp-13)

(defn función-comp-14
  []
  (let [f (fn [x] (integer? x))
        g (fn [xs] (first xs))
        h (comp f g)]
    (h [\1 \b])))

(función-comp-14)

(defn función-comp-15
  []
  (let [f (fn [x] (* 3 x))
        g (fn [xs] (+ 2 xs))
        h (comp f g)]
    (h 10)))

(función-comp-15)

(defn función-comp-16
  []
  (let [f (fn [x] (- 5 x))
        g (fn [xs] (* 4 xs))
        h (comp f g)]
    (h 2)))

(función-comp-16)

(defn función-comp-17
  []
  (let [f (fn [x] (+ 12 x))
        g (fn [xs] (+ 34 xs))
        h (comp f g)]
    (h 10)))

(función-comp-17)

(defn función-comp-18
  []
  (let [f (fn [x] (- 10 x))
        g (fn [xs] (- 3 xs))
        h (comp f g)]
    (h 25)))

(función-comp-18)

(defn función-comp-19
  []
  (let [f (fn [x] (- x 50))
        g (fn [xs] (* 3 xs))
        h (comp f g)]
    (h 100)))

(función-comp-19)

(defn función-comp-20
  []
  (let [f (fn [x] (* x 2))
        g (fn [xs] (* xs 3))
        h (comp f g)]
    (h 10)))

(función-comp-20)

(defn función-complement-1
  []
  (let [f (fn [xs] (map even? xs))
        h (complement f)]
    (h '(1 2 3 4))))

(función-complement-1)

(defn función-complement-2
  []
  (let [f (fn [x] (float? x))
        h (complement f)]
    (h 1)))

(función-complement-2)

(defn función-complement-3
  []
  (let [f (fn [x] (associative? x))
        h (complement f)]
    (h "hola")))

(función-complement-3)

(defn función-complement-4
  []
  (let [f (fn [x] (boolean? x))
        h (complement f)]
    (h (new Boolean "true"))))

(función-complement-4)

(defn función-complement-5
  []
  (let [f (fn [x] (coll? x))
        h (complement f)]
    (h '("plf" "hola" "hello bro!"))))

(función-complement-5)

(defn función-complement-6
  []
  (let [f (fn [x] (map-entry? x))
        h (complement f)]
    (h (last [1 2 2]))))

(función-complement-6)

(defn función-complement-7
  []
  (let [f (fn [x] (map? x))
        h (complement f)]
    (h (inc 10))))

(función-complement-7)

(defn función-complement-8
  []
  (let [f (fn [x] (nat-int? x))
        h (complement f)]
    (h (+ 10 1))))

(función-complement-8)

(defn función-complement-9
  []
  (let [f (fn [x] (number? x))
        h (complement f)]
    (h 100)))

(función-complement-9)

(defn función-complement-10
  []
  (let [f (fn [x] (number? x))
        h (complement f)]
    (h (last [1 2 3]))))

(función-complement-10)

(defn función-complement-11
  []
  (let [f (fn [x] (pos-int? x))
        h (complement f)]
    (h 10M)))

(función-complement-11)

(defn función-complement-12
  []
  (let [f (fn [x] (rational? x))
        h (complement f)]
    (h (+ 2 1/4 3/8))))

(función-complement-12)

(defn función-complement-13
  []
  (let [f (fn [x] (boolean? x))
        g (complement f)]
    (g true)))

(función-complement-13)

(defn función-complement-14
  []
  (let [f (fn [x] (float? x))
        g (complement f)]
    (g 20)))

(función-complement-14)

(defn función-complement-15
  []
  (let [f (fn [x] (pos? x))
        g (complement f)]
    (g 15)))

(función-complement-15)

(defn función-complement-16
  []
  (let [f (fn [x] (odd? x))
        g (complement f)]
    (g 25)))

(función-complement-16)

(defn función-complement-17
  []
  (let [f (fn [x] (odd? x))
        g (complement f)]
    (g 53)))

(función-complement-17)

(defn función-complement-18
  []
  (let [f (fn [x] (associative? x))
        g (complement f)]
    (g [1 3 5 7])))

(función-complement-18)

(defn función-complement-19
  []
  (let [f (fn [x] (char? x))
        g (complement f)]
    (g \e)))

(función-complement-19)

(defn función-complement-20
  []
  (let [f (fn [x] (keyword? x))
        g (complement f)]
    (g :hola)))

(función-complement-20)

(defn función-constantly-1
  []
  (let [x true
        f (constantly x)]
    (f 1)))

(función-constantly-1)

(defn función-constantly-2
  []
  (let [x "Constante"
        f (constantly x)]
    (f [1 3894 4])))

(función-constantly-2)

(defn función-constantly-3
  []
  (let [x 123
        f (constantly x)]
    (f  {:hola "Hola" :mundo "Mundo"})))

(función-constantly-3)

(defn función-constantly-4
  []
  (let [x \U
        f (constantly x)]
    (f  {:Assignature "PLF" :horario "5-6"})))

(función-constantly-4)

(defn función-constantly-5
  []
  (let [x \U
        f (constantly x)]
    (f  {:Name "Jose"})))

(función-constantly-5)

(defn función-constantly-6
  []
  (let [x [:a "Buenas " :b "Noches"]
        f (constantly x)]
    (f  #{1 2 3 4 68 612})))

(función-constantly-6)

(defn función-constantly-7
  []
  (let [x (take 3 (iterate inc 10))
        f (constantly x)]
    (f  #{1 2 3 4 68 612})))

(función-constantly-7)

(defn función-constantly-8
  []
  (let [x (take 3 (iterate dec 10))
        f (constantly x)]
    (f  #{87 612 5 624})))

(función-constantly-8)

(defn función-constantly-9
  []
  (let [x (char? \s)
        f (constantly x)]
    (f  (take 5 (iterate inc 20)))))

(función-constantly-9)

(defn función-constantly-10
  []
  (let [x (coll? [1 2 3 4])
        f (constantly x)]
    (f  20)))

(función-constantly-10)

(defn función-constantly-11
  []
  (let [x 1.9M
        f (constantly x)]
    (f  decimal? 20)))

(función-constantly-11)

(defn función-constantly-12
  []
  (let [x 0.873
        f (constantly x)]
    (f  float? 40)))

(función-constantly-12)

(defn función-constantly-13
  []
  (let [x "Integer"
        f (constantly x)]
    (f  int? 54)))

(función-constantly-13)

(defn función-constantly-14
  []
  (let [x [:a \a :b \b :c \c :e \e]
        f (constantly x)]
    (f  int 54M)))

(función-constantly-14)

(defn función-constantly-15
  []
  (let [x  1231245125412512
        f (constantly x)]
    (f  rational? 200)))

(función-constantly-15)

(defn función-constantly-16
  []
  (let [x  "Constantly 16"
        f (constantly x)]
    (f  rational? [\a "g"])))

(función-constantly-16)

(defn función-constantly-17
  []
  (let [x  false
        f (constantly x)]
    (f  [2 3 54 5])))

(función-constantly-17)

(defn función-constantly-18
  []
  (let [x  [true true]
        f (constantly x)]
    (f  1 2 3)))

(función-constantly-18)

(defn función-constantly-19
  []
  (let [x  [true false false true]
        f (constantly x)]
    (f boolean?  true 1 2 false)))

(función-constantly-19)

(defn función-constantly-20
  []
  (let [x  25
        f (constantly x)]
    (f * 4 5)))

(función-constantly-20)

(defn funcion-every-pred-1
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (number? (f y)))
        z (every-pred f)]
    (z 3)))

(funcion-every-pred-1)

(defn funcion-every-pred-2
  []
  (let [f (fn [y] ((comp (partial > 5) count) y))
        g (fn [x] (string? x))
        z (every-pred g f)]
    (z "abc")))

(funcion-every-pred-2)

(defn funcion-every-pred-3
  []
  (let [f (fn [x] (number? x))
        g (fn [y] (pos? y))
        z (every-pred f g)]
    (z 10)))

(funcion-every-pred-3)

(defn funcion-every-pred-4
  []
  (let [f (fn [x] (char? x))
        g (fn [y] (char? y))
        z (every-pred f g)]
    (z \a)))

(funcion-every-pred-4)

(defn funcion-every-pred-5
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (pos? y))
        z (every-pred f g)]
    (z 3)))

(funcion-every-pred-5)

(defn funcion-every-pred-6
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (neg? y))
        z (every-pred f g)]
    (z -3)))

(funcion-every-pred-6)

(defn funcion-every-pred-7
  []
  (let [f (fn [x] (even? x))
        g (fn [y] (pos? y))
        z (every-pred f g)]
    (z 2)))

(funcion-every-pred-7)

(defn funcion-every-pred-8
  []
  (let [f (fn [x] (even? x))
        g (fn [y] (neg? y))
        z (every-pred f g)]
    (z -2)))

(funcion-every-pred-8)

(defn funcion-every-pred-9
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (double? y))
        z (every-pred f g)]
    (z 20.10)))

(funcion-every-pred-9)

(defn funcion-every-pred-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (double? y))
        z (every-pred f g)]
    (z 20.10)))

(funcion-every-pred-10)

(defn funcion-every-pred-11
  []
  (let [f (fn [x] (true? x))
        g (fn [y] (boolean? y))
        z (every-pred f g)]
    (z true)))

(funcion-every-pred-11)

(defn funcion-every-pred-12
  []
  (let [f (fn [x] (false? x))
        g (fn [y] (boolean? y))
        z (every-pred f g)]
    (z false)))

(funcion-every-pred-12)

(defn funcion-every-pred-13
  []
  (let [f (fn [x] (false? x))
        g (fn [y] (boolean? y))
        z (every-pred f g)]
    (z false)))

(funcion-every-pred-13)

(defn funcion-every-pred-14
  []
  (let [f (fn [x] (number? x))
        g (fn [y] (char? y))
        z (every-pred f g)]
    (z \1)))

(funcion-every-pred-14)

(defn funcion-every-pred-15
  []
  (let [f (fn [x] (keyword? x))
        g (fn [y] (symbol? y))
        z (every-pred f g)]
    (z (:a "abc"))))

(funcion-every-pred-15)

(defn funcion-every-pred-16
  []
  (let [f (fn [x] (indexed? x))
        g (fn [y] (map? y))
        z (every-pred f g)]
    (z '(:a 1 :b 2))))

(funcion-every-pred-16)

(defn funcion-every-pred-17
  []
  (let [f (fn [x] (vector? x))
        g (fn [y] (associative? y))
        z (every-pred f g)]
    (z [1 2 3])))

(funcion-every-pred-17)

(defn funcion-every-pred-18
  []
  (let [f (fn [x] (map? x))
        g (fn [y] (associative? y))
        z (every-pred f g)]
    (z {:a 1 :b 2})))

(funcion-every-pred-18)

(defn funcion-every-pred-19
  []
  (let [f (fn [x] (set? x))
        g (fn [y] (associative? y))
        z (every-pred f g)]
    (z #{:a :b})))

(funcion-every-pred-19)

(defn funcion-every-pred-20
  []
  (let [f (fn [x] (list? x))
        g (fn [y] (associative? y))
        z (every-pred f g)]
    (z '(1 2 3))))

(funcion-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [x] (str "Hola " x))
        y (fnil f "mundo")
        z (fnil f "a todos")]
    [(y "Jose") (z "Hernandez")]))

(función-fnil-1)

(defn función-fnil-2
  []
  (let [f (fn [x y] (+ x y))
        z (fnil f nil nil)]
    (z 30 30)))

(función-fnil-2)

(defn función-fnil-3
  []
  (let [f (fn [name] (str "Hola " name))
        z (fnil f "")]
    (z "Mundo")))

(función-fnil-3)

(defn función-fnil-4
  []
  (let [f (fn [x y] (str "Dos mascotas: " x " y " y))
        z (fnil f "tortuga" "hamster")]
    (z "perro" "gato")))

(función-fnil-4)

(defn función-fnil-5
  []
  (let [f (fn  [first] (str "Hola" first))
        z (fnil f "")]
    (z "Mundo")))

(función-fnil-5)

(defn función-fnil-6
  []
  (let [f (fn [x] (map true? x))
        z (fnil f (map true? [false true false true]))]
    (z ["true" true true nil])))

(función-fnil-6)

(defn función-fnil-7
  []
  (let [f (fn [x] (nil? x))
        z (fnil f (nil? nil))]
    (z nil)))

(función-fnil-7)

(defn función-fnil-8
  []
  (let [f (fn [x] {:a x} [:b])
        z (fnil f nil)]
    (z 5)))

(función-fnil-8)

(defn función-fnil-9
  []
  (let [f (fn [x] (take-last 10 x))
        z (fnil f (take-last 10 (range 50 100)))]
    (z '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20))))

(función-fnil-9)

(defn función-fnil-10
  []
  (let [f (fn [x] (take-last 10 x))
        y (fnil f (take-nth 100 (range 100 500)))
        z (fnil f (take-last 10 (range 50 100)))]
    [(y nil) (z nil)]))

(función-fnil-10)

(defn función-fnil-11
  []
  (let [f (fn [x] (take-last 3 x))
        z (fnil f (take 2 (range 10)))]
    (z [\a \e \i \o \u])))

(función-fnil-11)

(defn función-fnil-12
  []
  (let [f (fn [x] (map boolean? x))
        z (fnil f "x es nil")]
    (z #{false true})))

(función-fnil-12)

(defn función-fnil-13
  []
  (let [f (fn [x] (boolean? x))
        z (fnil f "x es nil")]
    (z nil)))

(función-fnil-13)

(defn función-fnil-14
  []
  (let [f (fn [x] (reverse x))
        z (fnil f (+ 2 3))]
    (z "Hola Mundo")))

(función-fnil-14)

(defn función-fnil-15
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (first x))
        y (fnil f nil)
        z (fnil g nil)]
    [(y [1 2 3 4 5]) (z [5 4 3 2 1])]))

(función-fnil-15)

(defn función-fnil-16
  []
  (let [f (fn [x] (str "Mi color favorito es el " x))
        z (fnil f "azul")]
    (z "morado")))

(función-fnil-16)

(defn función-fnil-17
  []
  (let [f (fn [x] (str "Mi color favorito es el " x))
        z (fnil f "morado")]
    (z nil)))

(función-fnil-17)

(defn función-fnil-18
  []
  (let [f (fn [w x y] (replace [:primero :segundo :tercero :cuarto :cinco :seis] [w x y]))
        z (fnil f 1 1 1)]
    (z nil nil nil)))

(función-fnil-18)

(defn función-fnil-19
  []
  (let [f (fn [w x y] (replace [\a \b \c \d \e \f \g \h \i \j \k \l \m \n \ñ] [w x y]))
        z (fnil f 3 7 11)]
    (z nil 14 nil)))

(función-fnil-19)

(defn función-fnil-20
  []
  (let [f (fn [a] (inc a))
        g (fn [a b] (* a b))
        h (fn [a b c] (+ a b c))
        x (fnil f 110)
        y (fnil g 1/2 1/8)
        z (fnil h 98 12 897)]
    [(x nil) (y 6 nil) (z 8/5 nil 3/4)]))

(función-fnil-20)

(defn función-juxt-1
  []
  (let [f (fn [x] (first x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "Jose")))

(función-juxt-1)

(defn función-juxt-2
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (drop 4 x))
        z (juxt f g)]
    (z [1 2 3 4 5 6])))

(función-juxt-2)

(defn función-juxt-3
  []
  (let [f (fn [x] (first x))
        g (fn [x] (str x))
        z (juxt g f)]
    (z [4 5 6 7])))

(función-juxt-3)

(defn función-juxt-4
  []
  (let [f (fn [x] (map inc x))
        g (fn [x] (filter odd? x))
        z (juxt g f)]
    (z #{3 4 5 6})))

(función-juxt-4)

(defn función-juxt-5
  []
  (let [f (fn [x] (count x))
        g (fn [x] (first x))
        z (juxt g f)]
    (z [1 3 5 6 7])))

(función-juxt-5)

(defn función-juxt-6
  []
  (let [f (fn [x] (+ 3 x))
        g (fn [x] (* 1 x))
        h (fn [x] (dec  x))
        z (juxt h g f)]
    (z 10)))

(función-juxt-6)

(defn función-juxt-7
  []
  (let [f (fn [x] (filter char? x))
        g (fn [x] (count x))
        h (fn [x] (vector x))
        z (juxt h g f)]
    (z '(1 \a "hola" \b 2 "tres" \x))))

(función-juxt-7)

(defn función-juxt-8
  []
  (let [f (fn [x] (last x))
        g (fn [x] (list x))
        h (fn [x] (conj [1000] x))
        z (juxt h g f)]
    (z [4 2 1 6 8 10 15 33 100 5])))

(función-juxt-8)

(defn función-juxt-9
  []
  (let [f (fn [x] (take 5 x))
        g (fn [x] (reverse x))
        z (juxt g f)]
    (z "Hola me llamo Jose")))

(función-juxt-9)

(defn función-juxt-10
  []
  (let [f (fn [x] (range x))
        g (fn [x] (+ 40 x))
        h (fn [x] (int? x))
        z (juxt f h g)]
    (z 12)))

(función-juxt-10)

(defn función-juxt-11
  []
  (let [f (fn [x] (map key x))
        g (fn [x] (map val x))
        z (juxt f g)]
    (z {:a 2 :b 4 :c 6 :d 8})))

(función-juxt-11)

(defn función-juxt-12
  []
  (let [f (fn [x] (count x))
        g (fn [x] (:a :b x))
        z (juxt f g)]
    (z {:a 2 :b 4 :c 6 :d 8})))

(función-juxt-12)

(defn función-juxt-13
  []
  (let [f (fn [x] (sort x))
        g (fn [x] (conj [100 200] x))
        z (juxt f g)]
    (z "abcdaabccc")))

(función-juxt-13)

(defn función-juxt-14
  []
  (let [f (fn [x xy xz] (max x xy xz))
        g (fn [x xy xz] (min x xy xz))
        h (fn [x xy xz] (+ x xy xz))
        z (juxt f g h)]
    (z  10 4 12)))

(función-juxt-14)

(defn función-juxt-15
  []
  (let [f (fn [x] (remove true? x))
        g (fn [x] (last x))
        h (fn [x] (filter boolean? x))
        z (juxt g f h)]
    (z '(1 -2 true 2 -1 false 3 7 false 0 true true))))

(función-juxt-15)

(defn función-juxt-16
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (list? x))
        h (fn [x] (map? x))
        z (juxt g f h)]
    (z '("juan" "isra" "jose" "karen"))))

(función-juxt-16)

(defn función-juxt-17
  []
  (let [f (fn [x] (take-last 2 x))
        g (fn [x] (update x 1 inc))
        h (fn [x] (count x))
        z (juxt g f h)]
    (z [24 25 26 27])))

(función-juxt-17)

(defn función-juxt-18
  []
  (let [f (fn [x] (drop-last x))
        g (fn [x] (remove pos? x))
        z (juxt g f)]
    (z #{1 0 -2 4 5 -10 -30 8 10 -1})))

(función-juxt-18)

(defn función-juxt-19
  []
  (let [f (fn [x] (sort-by count x))
        g (fn [x] (string? x))
        z (juxt f g)]
    (z ["abc" "a" "b" "cd"])))

(función-juxt-19)

(defn función-juxt-20
  []
  (let [f (fn [x xs] (+ x xs))
        g (fn [x xs] (- x xs))
        h (fn [x xs] (/ x xs))
        z (juxt g f h)]
    (z 10 14)))

(función-juxt-20)

(defn función-partial-1
  []
  (let [f (fn [xs] (+ 100 xs))
        z (partial f)]
    (z 5)))

(función-partial-1)

(defn función-partial-2
  []
  (let [f (fn [xs] (* 100 xs))
        z (partial f)]
    (z 5)))

(función-partial-2)

(defn función-partial-3
  []
  (let [f (fn [xs] (- 100 xs))
        z (partial f)]
    (z 10)))

(función-partial-3)

(defn función-partial-4
  []
  (let [f (fn [xs] (/ 100 xs))
        z (partial f)]
    (z 5)))

(función-partial-4)

(defn función-partial-5
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f)]
    (z 1 2 3)))

(función-partial-5)

(defn función-partial-6
  []
  (let [f (fn [x y z] (vector x y z))
        z (partial f)]
    (z 4 5 6)))

(función-partial-6)

(defn función-partial-7
  []
  (let [f (fn [x y z] (list x y z))
        z (partial f)]
    (z 7 8 9)))

(función-partial-7)

(defn función-partial-8
  []
  (let [f (fn [w x y z] (hash-map w x y z))
        z (partial f)]
    (z 10 20 30 40)))

(función-partial-8)

(defn función-partial-9
  []
  (let [f (fn [xs] (filter char? xs))
        z (partial f)]
    (z "hola")))

(función-partial-9)

(defn función-partial-10
  []
  (let [f (fn [xs] (group-by char? xs))
        z (partial f)]
    (z "12345")))

(función-partial-10)

(defn función-partial-11
  []
  (let [f (fn [xs] (sort-by char? xs))
        z (partial f)]
    (z "hola")))

(función-partial-11)

(defn función-partial-12
  []
  (let [f (fn [xs] (take-while char? xs))
        z (partial f)]
    (z "hola")))

(función-partial-12)

(defn función-partial-13
  []
  (let [f (fn [xs] (sequential? xs))
        z (partial f)]
    (z "hola")))

(función-partial-13)

(defn función-partial-14
  []
  (let [f (fn [xs] (remove char? xs))
        z (partial f)]
    (z "hola")))

(función-partial-14)

(defn función-partial-15
  []
  (let [f (fn [xs] (filterv even? xs))
        z (partial f)]
    (z [10 20 30 40])))

(función-partial-15)

(defn función-partial-16
  []
  (let [f (fn [xs] (filter neg? xs))
        z (partial f)]
    (z [-10 20 -30 40])))

(función-partial-16)

(defn función-partial-17
  []
  (let [f (fn [xs] (filter pos? xs))
        z (partial f)]
    (z [-10 20 -30 40])))

(función-partial-17)

(defn función-partial-18
  []
  (let [f (fn [xs] (take-while string? xs))
        z (partial f)]
    (z ["hola" -10 20 -30 40])))

(función-partial-18)

(defn función-partial-19
  []
  (let [f (fn [xs] (take-while number? xs))
        z (partial f)]
    (z [-10 5.5 -30 false true])))

(función-partial-19)

(defn función-partial-20
  []
  (let [f (fn [xs] (take-while boolean? xs))
        z (partial f)]
    (z [false true -10 5.5])))

(función-partial-20)

(defn funcion-some-fn-1
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z 10)))

(funcion-some-fn-1)

(defn funcion-some-fn-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -7)))

(funcion-some-fn-2)

(defn funcion-some-fn-3
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -7)))

(funcion-some-fn-3)

(defn funcion-some-fn-4
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z -4)))

(funcion-some-fn-4)

(defn funcion-some-fn-5
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -4)))

(funcion-some-fn-5)

(defn funcion-some-fn-6
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -4)))

(funcion-some-fn-6)

(defn funcion-some-fn-7
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z 10)))

(funcion-some-fn-7)

(defn funcion-some-fn-8
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -7)))

(funcion-some-fn-8)

(defn funcion-some-fn-9
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -7)))

(funcion-some-fn-9)

(defn funcion-some-fn-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z -4)))

(funcion-some-fn-10)

(defn funcion-some-fn-11
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -4)))

(funcion-some-fn-11)

(defn funcion-some-fn-12
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -4)))

(funcion-some-fn-12)

(defn funcion-some-fn-13
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (decimal? y))
        z (some-fn f g)]
    (z -4)))

(funcion-some-fn-13)

(defn funcion-some-fn-14
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z -4)))

(funcion-some-fn-14)

(defn funcion-some-fn-15
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (double? y))
        z (some-fn f g)]
    (z 4)))

(funcion-some-fn-15)

(defn funcion-some-fn-16
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (decimal? y))
        z (some-fn f g)]
    (z 4)))

(funcion-some-fn-16)

(defn funcion-some-fn-17
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 4)))

(funcion-some-fn-17)

(defn funcion-some-fn-18
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (double? y))
        z (some-fn f g)]
    (z -4)))

(funcion-some-fn-18)

(defn funcion-some-fn-19
  []
  (let [f (fn [x] (even? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z -4)))

(funcion-some-fn-19)

(defn funcion-some-fn-20
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z -4)))

(funcion-some-fn-20)
